'use strict';

angular.module('todoWebApp')
    .controller('TodosCtrl', function($scope, TodoService) {
        $scope.items = [];
        $scope.pendingItems = []
        $scope.completedItems = [];
        $scope.itemsLeftCounter = 0;
        $scope.newTodo = '';
        $scope.editingTodo = -1;
        $scope.selectedFilter = 'all';
        $scope.syncing = false;

        function createGuid()
        {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }

        var pushPending = function(task,uuid){
            var objectTask = {task:task,done:false,uuid:uuid};
            $scope.pendingItems.push(objectTask);
            $scope.items.push(objectTask);
            $scope.itemsLeftCounter += 1;
            $scope.newTodo = '';
            $scope.syncing=true;
        };

        var alterPending = function(task){
            $scope.pendingItems.push(task);
            $scope.syncing=true;
        };

        var removePendingByUUID = function(uuid) {
            $scope.pendingItems = $scope.pendingItems.filter(function (obj) {
                return obj.uuid!=uuid;
            });
            if ($scope.pendingItems.length === 0){
                $scope.syncing = false;
            }
        };

        TodoService.list().then(function (result) {
            if (result.data.constructor === Array) {
                $scope.syncing = false;
                $scope.items = result.data;
                $scope.itemsLeftCounter = $scope.items.reduce(function (prev, current, index, array) {
                    if (!array[index].done) {
                        return prev + 1;
                    }
                    return prev;
                }, 0);
            }
        });

        $scope.addTodo = function () {
            var task = $scope.newTodo.trim();
            if (task === '') {
                return;
            }
            var uuid = createGuid();
            pushPending(task,uuid);
            TodoService.add(task, uuid)
                .then(function (result) {
                    //fetch pending off pending queue
                    removePendingByUUID(result.data.uuid);
                    //and replace item in items queue
                    var index = $scope.items.map(function(e) { return e.uuid; }).indexOf(result.data.uuid);
                    for(var k in result.data) $scope.items[index][k]=result.data[k];
//                    $scope.items[index] = result.data;
                });
        };

        $scope.toggleTodo = function (todo_id, index) {
            $scope.items[index]['done'] = !$scope.items[index]['done'];
            if ($scope.items[index]['done']) {
                $scope.itemsLeftCounter -= 1;
            } else {
                $scope.itemsLeftCounter += 1;
            }
            alterPending($scope.items[index]);
            TodoService.update(todo_id, {done: $scope.items[index]['done']})
                .then(function () {
                    removePendingByUUID($scope.items[index]['uuid']);
                });
        };

        $scope.removeTodo = function (todo_id, index) {
            alterPending($scope.items[index]);
            $scope.items.splice(index, 1);
            TodoService.remove(todo_id).then(
                function() {
                    removePendingByUUID(todo_id);
                }
            );
        };

        $scope.editTodo = function (todo_id) {
            $scope.editingTodo = todo_id;
        };

        $scope.doneEditingTodo = function (todo_id, index) {
            $scope.closeEditingTodo();
            alterPending($scope.items[index]);
            TodoService.update(todo_id, {task: $scope.items[index]['task']})
                .then(function () {
                    removePendingByUUID($scope.items[index]['uuid']);
                });
        };

        $scope.closeEditingTodo = function () {
            $scope.editingTodo = -1;
        };

        $scope.clearCompleted = function () {
            TodoService.clearCompleted()
                .then(function() {
                   $scope.items = $scope.items.filter(function (el) {
                       return !el.done;
                   });
                   $scope.itemsLeftCounter = $scope.items.length;
                });
        };

        $scope.markAllComplete = function () {
            TodoService.markAllComplete()
                .then(function () {
                    $scope.items.forEach(function(el) {
                        el.done = true;
                    });
                });
        }
    });