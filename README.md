TodoApp
=======

A simple Todo Application made with Angular.js (client side) and Flask as Server.
Modification of https://github.com/vinceprignano/todoapp to work on AWS
Using Zappa to deploy on AWS Lambda and PynamoDB to store data on Dynamo


## Requirements
- ``` pip install -r requirements.txt ```
- more instructions coming soon
